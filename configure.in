dnl 
dnl configure.in
dnl
dnl Copyright (c) 2001 Dug Song <dugsong@monkey.org>
dnl
dnl $Id: configure.in,v 1.18 2002/04/13 05:20:49 dugsong Exp $

AC_INIT(fragroute.c)

AM_INIT_AUTOMAKE(fragroute, 1.2)
AM_CONFIG_HEADER(config.h)

dnl FR_WITH_LIBRARY (LIBRARY-NAME, CONSTANT-PREFIX)
dnl   This macro will declare a --with-lib<LIBRARY-NAME> which will
dnl   take the directory name for this library.  By default, it will
dnl   ask CPP and CC to check for the existence of the header file and
dnl   the library itself.  If a directory name is specified, it tries
dnl   searching some default subdirectories for both the header and
dnl   the library.

AC_DEFUN(FR_WITH_LIBRARY,
[
$2INC=""
$2LIB="-l$1"
AC_ARG_WITH(lib$1,
            [  --with-lib$1=DIR     use lib$1 in DIR],
	    [case "$withval" in
             yes)
	        AC_CHECK_HEADER($1.h,
	                        ,
                                [AC_ERROR($1.h not found)])
                AC_CHECK_LIB($1, main,
                             ,
                             [AC_ERROR(lib$1 not found)])
                ;;
             no)
                AC_ERROR(lib$1 is mandatory)
                ;;
             *)
                AC_MSG_CHECKING(for $1.h)
                withval=`echo $withval | sed -e 's/\/$//'` # Remove trailing /
                if test -f $withval/include/$1.h; then
                   AC_MSG_RESULT([yes
   $withval/include/])
                   $2INC="-I$withval/include"
                elif test -f $withval/$1.h; then
                   AC_MSG_RESULT([yes
   $withval/])
                   $2INC="-I$withval"
                else
                   AC_MSG_RESULT([no
   $withval/])
                   AC_ERROR($1.h not found)
                fi
                #
                AC_CHECK_LIB($1, main,
                             [AC_MSG_RESULT([   $withval/lib/])
                              $2LIB="-L$withval/lib/ $$2LIB"
                             ],
                             [AC_CHECK_LIB($1, main,
                                           [AC_MSG_RESULT([   $withval/])
                                            $2LIB="-L$withval/ $$2LIB"
                                           ],
                                           [AC_ERROR(lib$1 not found)],
                                           -L$withval)
                             ],
                             -L$withval/lib)
             esac],
	    [AC_CHECK_HEADER($1.h,
	                     ,
                             [AC_ERROR($1.h not found)])
             AC_CHECK_LIB($1, main,
                          ,
                          [AC_ERROR(lib$1 not found)])
            ])
AC_SUBST($2INC)
AC_SUBST($2LIB)
]
)

dnl Initialize prefix.
if test "$prefix" = "NONE"; then
   prefix="/usr/local"
fi

dnl Checks for programs.
AC_PROG_AWK
AC_PROG_CC
AC_PROG_CPP
AC_PROG_INSTALL
AC_PROG_LN_S
AC_PROG_RANLIB

dnl XXX - we need MingW32 under Cygwin for win32
AC_CYGWIN
if test "$CYGWIN" = yes ; then
   if test -d /usr/include/mingw; then
      CPPFLAGS="$CPPFLAGS -mno-cygwin"
      CFLAGS="$CFLAGS -mno-cygwin"
      AC_DEFINE(WIN32_LEAN_AND_MEAN, 1, 
          [Define for faster code generation.])
      AC_DEFINE(snprintf, _snprintf, [Use MingW32's internal snprintf])
      AC_DEFINE(vsnprintf, _vsnprintf, [Use MingW32's internal vsnprintf])
   else
      AC_MSG_ERROR([need MingW32 package to build under Cygwin])
   fi
   dnl XXX - use our internal event-win32 hack
   EVENTINC="-I${srcdir}/win32"
   LIBOBJS="$LIBOBJS event-win32.o"
   AC_SUBST(EVENTINC)

   dnl Checks for winpcap
   FR_WITH_LIBRARY(wpcap, PCAP)

else
dnl XXX - BEGIN !CYGWIN

   AC_LBL_LIBRARY_NET

   dnl Checks for libevent
   FR_WITH_LIBRARY(event, EVENT)

   dnl Checks for libpcap
   FR_WITH_LIBRARY(pcap, PCAP)

dnl XXX - END !CYGWIN
fi

FR_WITH_LIBRARY(dumbnet, DUMBNET)

dnl Check for tunnel interface
AC_MSG_CHECKING(for tunnel interface)
if test -c /dev/ip -a -c /dev/tun -a -f /usr/include/net/if_tun.h; then
   AC_MSG_RESULT(Universal TUN/TAP driver on Solaris)
   LIBOBJS="$LIBOBJS tun-solaris.o"
elif test "$CYGWIN" = yes ; then
   AC_MSG_RESULT(win32 - assume CIPE driver is installed)
   LIBOBJS="$LIBOBJS tun-win32.o"
else
   AC_MSG_RESULT(using loopback)
   LIBOBJS="$LIBOBJS tun-loop.o"
fi

dnl Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS(fcntl.h inttypes.h malloc.h stdint.h strings.h)
AC_CHECK_HEADERS(sys/ioctl.h sys/time.h unistd.h)
AC_CHECK_HEADERS(windows.h winsock.h)

dnl Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_TYPE_OFF_T
AC_TYPE_SIZE_T
AC_HEADER_TIME
AC_STRUCT_TM
AC_PROG_GCC_TRADITIONAL
if test "$GCC" = yes; then
   CFLAGS="$CFLAGS -Wall"
fi

dnl Checks for library functions.
AC_TYPE_SIGNAL
AC_CHECK_FUNCS(arc4random gettimeofday select strdup)
AC_REPLACE_FUNCS(getopt strlcat strlcpy)

dnl Set system-wide configuration file.
FRAGROUTE_CONF=`eval echo ${sysconfdir}/fragroute.conf`
AC_SUBST(FRAGROUTE_CONF)
AC_DEFINE_UNQUOTED(FRAGROUTE_CONF, "$FRAGROUTE_CONF",
	[Location of configuration file.])

AC_OUTPUT(Makefile scripts/Makefile win32/Makefile fragroute.8)
