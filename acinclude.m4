dnl
dnl AC_FR_WITH_LIBRARY (LIBRARY-NAME, CONSTANT-PREFIX)
dnl
dnl This macro will declare a --with-lib<LIBRARY-NAME> which will
dnl take the directory name for this library.  By default, it will
dnl ask CPP and CC to check for the existence of the header file and
dnl the library itself.  If a directory name is specified, it tries
dnl searching some default subdirectories for both the header and
dnl the library.
dnl
AC_DEFUN(AC_FR_WITH_LIBRARY,
[
# We export two variables: CONSTANT-PREFIXINC and CONSTANT-PREFIXLIB
# so that they can be passed to CC.
$2INC=""
$2LIB="-l$1"
AC_ARG_WITH(lib$1,
            [  --with-lib$1=DIR     use lib$1 in DIR],
            [case "$withval" in
             yes)
                # We work as normal, as if --with-libLIBRARY-NAME was
                # not passed.
                AC_CHECK_HEADER($1.h,
                                ,
                                [AC_ERROR($1.h not found)])
                AC_CHECK_LIB($1, main,
                             ,
                             [AC_ERROR(lib$1 not found)])
                ;;
             no)
                # TODO We need better error handling here
                AC_ERROR(lib$1 is mandatory)
                ;;
             *)
                # A directory was specified.  We will try to search for
                # the headers, and then the library itself.
                AC_MSG_CHECKING(for $1.h)
                withval=`echo $withval | sed -e 's/\/$//'` # Remove trailing /
                if test -f $withval/include/$1.h; then
                   AC_MSG_RESULT([yes
   $withval/include/])
                   $2INC="-I$withval/include"
                elif test -f $withval/$1.h; then
                   AC_MSG_RESULT([yes
   $withval/])
                   $2INC="-I$withval"
                else
                   AC_MSG_RESULT([no
   $withval/])
                   AC_ERROR($1.h not found)
                fi
                #
                AC_CHECK_LIB($1, main,
                             [AC_MSG_RESULT([   $withval/lib/])
                              $2LIB="-L$withval/lib/ $$2LIB"
                             ],
                             [AC_CHECK_LIB($1, main,
                                           [AC_MSG_RESULT([   $withval/])
                                            $2LIB="-L$withval/ $$2LIB"
                                           ],
                                           [AC_ERROR(lib$1 not found)],
                                           -L$withval)
                             ],
                             -L$withval/lib)
             esac],
            [# Default behaviour.  Use Autoconf's built-ins.
             AC_CHECK_HEADER($1.h,
                             ,
                             [AC_ERROR($1.h not found)])
             AC_CHECK_LIB($1, main,
                          ,
                          [AC_ERROR(lib$1 not found)])
            ])
AC_SUBST($2INC)
AC_SUBST($2LIB)
]
)

dnl
dnl AC_LBL_LIBRARY_NET
dnl
dnl This test is for network applications that need socket() and
dnl gethostbyname() -ish functions.  Under Solaris, those applications
dnl need to link with "-lsocket -lnsl".  Under IRIX, they need to link
dnl with "-lnsl" but should *not* link with "-lsocket" because
dnl libsocket.a breaks a number of things (for instance:
dnl gethostbyname() under IRIX 5.2, and snoop sockets under most
dnl versions of IRIX).
dnl
dnl Unfortunately, many application developers are not aware of this,
dnl and mistakenly write tests that cause -lsocket to be used under
dnl IRIX.  It is also easy to write tests that cause -lnsl to be used
dnl under operating systems where neither are necessary (or useful),
dnl such as SunOS 4.1.4, which uses -lnsl for TLI.
dnl
dnl This test exists so that every application developer does not test
dnl this in a different, and subtly broken fashion.

dnl It has been argued that this test should be broken up into two
dnl seperate tests, one for the resolver libraries, and one for the
dnl libraries necessary for using Sockets API. Unfortunately, the two
dnl are carefully intertwined and allowing the autoconf user to use
dnl them independantly potentially results in unfortunate ordering
dnl dependancies -- as such, such component macros would have to
dnl carefully use indirection and be aware if the other components were
dnl executed. Since other autoconf macros do not go to this trouble,
dnl and almost no applications use sockets without the resolver, this
dnl complexity has not been implemented.
dnl
dnl The check for libresolv is in case you are attempting to link
dnl statically and happen to have a libresolv.a lying around (and no
dnl libnsl.a).
dnl
AC_DEFUN(AC_LBL_LIBRARY_NET, [
    # Most operating systems have gethostbyname() in the default searched
    # libraries (i.e. libc):
    AC_CHECK_FUNC(gethostbyname, ,
        # Some OSes (eg. Solaris) place it in libnsl:
        AC_CHECK_LIB(nsl, gethostbyname, , 
            # Some strange OSes (SINIX) have it in libsocket:
            AC_CHECK_LIB(socket, gethostbyname, ,
                # Unfortunately libsocket sometimes depends on libnsl.
                # AC_CHECK_LIB's API is essentially broken so the
                # following ugliness is necessary:
                AC_CHECK_LIB(socket, gethostbyname,
                    LIBS="-lsocket -lnsl $LIBS",
                    AC_CHECK_LIB(resolv, gethostbyname),
                    -lnsl))))
    AC_CHECK_FUNC(socket, , AC_CHECK_LIB(socket, socket, ,
        AC_CHECK_LIB(socket, socket, LIBS="-lsocket -lnsl $LIBS", ,
            -lnsl)))
    # DLPI needs putmsg under HPUX so test for -lstr while we're at it
    AC_CHECK_LIB(str, putmsg)
    ])
